/****************************
*		Modules
****************************/
//node modules
var http = require('http');							//transfer data over http
var fs = require('fs');								//file system

//config
var config = require('./config');					//configurations: HOST, PORT, MONGO_HOST, MONGO_PORT, MONGO_DB etc.

//custom modules
var dt = require('./modules/date');					//get date time
var pages = require('./modules/readfile');			//read pages
//var mongoDB = require('./modules/mongo');			//mongodb module
var q = require('q');								//run async calls in right order
var auth = require('./modules/auth');				//user authentication
var sha256 = require('./modules/sha256');			//hash strings & get random sha256 str
var schedule = require('./modules/schedule');		//clean sessions task
var live = require('./modules/broadcast');			//broadcast using websockets
var digitraffic = require('./modules/train');		//digitraffic

//express js & router
var cors = require('cors')
var router = require('./modules/router');			//handle all routes
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use( bodyParser.json() );       				// to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     				// to support URL-encoded bodies
  extended: true
}));

	app.use('/', router); 							//creates a new router, '/test' would apply the routes to /test/route etc..
	app.use(cors());								//cross-origin requests


/****************************
*		Set up Mongo
****************************/
//create database and tables
//mongoDB.exec("create", "users");
//mongoDB.exec("create", "sessions");

//test data
/*var myobj =
[
	{ name: 'Admin', 	password: sha256.hash('root1'),		role:'3'},
	{ name: 'Peter', 	password: sha256.hash('root2'),		role:'2'},
	{ name: 'Amy', 		password: sha256.hash('root3'),		role:'2'},
	{ name: 'Hannah', 	password: sha256.hash('root4'),		role:'1'}
];
mongoDB.exec("add", "users", myobj);*/
//mongoDB.exec("get", "users", {}, {});
//mongoDB.exec("del", "users", {name: "John"});
//mongoDB.exec("drop", "sessions");


/****************************
*		Schedules
****************************/
//var digitraffic_interval = setInterval( () => { digitraffic.fetchTrains('SLO') }, config.DIGITRAFFIC_UPDATE);


/****************************
*		Digitransit
****************************/
//digitraffic.fetchTrains('SLO');


/****************************
*		Start Server
****************************/
app.listen(config.PORT, function ()
{
	console.log('Server listening on port ' + config.PORT)
})
