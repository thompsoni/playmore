module.exports =
{
	HOST: "localhost",
	PORT: "8080",
	MONGO_HOST: "localhost",
	MONGO_PORT: "27017",
	MONGO_DB: "mydb",
	SESSION_LIFETIME: 24,										//in hours
	SESSION_INTERVAL: (1 * 60 * 60 * 1000),						//how often to check the sessions in MS
	DIGITRAFFIC_UPDATE: 5000									//how often to fetch data from digitraffic in ms
}
