var express = require('express');
var router = express.Router();
var url = require('url');
var sha256 = require('./sha256');
var auth = require('./auth');
var config = require('../config');
const monk = require('monk');
const dburl = config.MONGO_HOST + ':' + config.MONGO_PORT + '/' + config.MONGO_DB;
var digitraffic = require('./train');
var validator = require('validator');
//var bodyParser = require('body-parser');


/****************
*	DEFAULT		*
****************/
router.get('/', function (req, res, next)
{
		res.send('default');
});


/****************
*	AUTH		*
****************/
router.get('/auth', function (req, res, next)
{
	var urli = url.parse(req.url, true);
	//console.log(urli);

	var username = urli.query.user;
	var userpw = sha256.hash(urli.query.pw);
	//console.log("details: " + username + " " + userpw);

	//step 1 - auth user
	auth.login(username, userpw, function callback(authSuccess, userobj)
	{
		if(authSuccess === true)
		{
				console.log("auth success: " + authSuccess);

				//step 2 - check if session exists
				auth.sessionExists(userobj, function callback(sessionFound, userobj)
				{
					if(sessionFound === true)
					{
						console.log("session found: " + sessionFound);
						console.log("-> " + userobj[0].sid); //userobj here is session, not user
						res.setHeader('Access-Control-Allow-Origin','*');
						res.json({sid: userobj[0].sid})
					}
					else
					{
						console.log("session not found");

						//step 3 - add new session if doesn't exist for username
						auth.addSession(userobj, function callback(sessionAdded, sid)
						{
							if(sessionAdded === true)
							{
								console.log("->session added: " + sid);
								res.setHeader('Access-Control-Allow-Origin','*');
								res.json({sid: sid})
							}
							else
							{
								console.log("->session not added: ");
								res.setHeader('Access-Control-Allow-Origin','*');
								res.json({sid: ''})
							}
						});
					}
				});
			}
			else
			{
				console.log("auth failed");
				res.setHeader('Access-Control-Allow-Origin','*');
				res.json({sid: ''})
			}

	});


});


/****************
*	Verify cookie
****************/
router.get('/verify', function (req, res, next)
{

	var urli = url.parse(req.url, true);
	var sid = urli.query.sid;

	auth.verifySession(sid, (verified, session) =>
	{
		if( verified === true )
		{
			console.log("Session verified for user: " + session[0].name);
			res.setHeader('Access-Control-Allow-Origin','*');
			res.send('verified');
		}
		else
		{
			console.log("Session not verified");
			res.setHeader('Access-Control-Allow-Origin','*');
			res.send('unverified');
		}
	});

});


function prepareComposition(trainNumber, stationShortCode, callback)
{

	var resultData = new Array();

	var loops = trainNumber.length;
	var timeout = setTimeout( function(){}, 10000 );

	digitraffic.fetchTrains(trainNumber, stationShortCode, function(liveTrains)
	{

		if( liveTrains === null || liveTrains === 'undefined' ){ console.log("fetchtrains returned null"); callback(null); return; }


		resultData.push(liveTrains);

		loops--;
		if( loops <= 0 )
		{
			console.log("FINAL");
			callback(resultData);
		}

		//if all trains not found, return
		clearTimeout(timeout);
		timeout = setTimeout( function()
		{
			if(loops > 0)
			{
				console.log("timeout"); callback(resultData);
			}

		}, 1000 );

	}); //livetrains



}


router.get('/train-composition', function (req, res, next)
{

	try
	{
		var trainNumber = new Array();
		var numsplit = req.query.trainNumber.split(',');
		var resultData = new Array();

		for( var i=0; i<numsplit.length; i++ )
		{
			if( validator.isNumeric(""+numsplit[i]) !== true ){ console.log("PARAM ERROR"); res.send( JSON.stringify("DATA ERROR") ); return;  }
			trainNumber.push( numsplit[i] );
		}

		if( validator.isAlpha(""+req.query.stationShortCode, 'en-US') !== true ){ console.log("PARAM ERROR2"); res.send( JSON.stringify("DATA ERROR") ); return;  }
		var stationShortCode = req.query.stationShortCode;


		prepareComposition(trainNumber, stationShortCode, function(composition)
		{
			//console.log(composition.length);

			if( composition != null )
			{

				for( var i=0; i<composition.length; i++ )
				{

					var comp = composition[i];
					if (typeof(comp.errorMessage) !== 'undefined')
					{
						res.send( comp.errorMessage );
						return;
					}
					//console.log(comp);

					resultData.push(
					{
						departureDate: comp.departureDate,
						trainName: comp.trainNumber,
						departureStation: comp.journeySections[0].beginTimeTableRow.stationUICCode,
						destinationStation: comp.journeySections[0].endTimeTableRow.stationUICCode,
						composition:
						{
							from: comp.journeySections[0].beginTimeTableRow.stationShortCode,
							to: comp.journeySections[0].endTimeTableRow.stationShortCode,
							wagons: comp.journeySections[0].wagons,
							locomotives: comp.journeySections[0].locomotives
						}

					});

				}

				res.send( JSON.stringify(resultData) );
			}
			else
			{
				res.send( JSON.stringify("DATA ERROR") );
			}

		});

	}
	catch(e)
	{
		console.log(e);
	}

});


router.post('/train-composition', function (req, res, next)
{
	var bodi = req.body;
	console.log(bodi);

	var trainNumber = new Array();
	var numsplit = req.body.trainNumber.split(',');
	var resultData = new Array();

	for( var i=0; i<numsplit.length; i++ )
	{
		if( validator.isNumeric(""+numsplit[i]) !== true ){ console.log("PARAM ERROR"); res.send( JSON.stringify("DATA ERROR") ); return;  }
		trainNumber.push( numsplit[i] );
	}

	if( validator.isAlpha(""+req.body.stationShortCode, 'en-US') !== true ){ console.log("PARAM ERROR2"); res.send( JSON.stringify("DATA ERROR") ); return;  }
	var stationShortCode = req.body.stationShortCode;


	prepareComposition(trainNumber, stationShortCode, function(composition)
	{
		//console.log(composition.length);

		if( composition != null )
		{

			for( var i=0; i<composition.length; i++ )
			{

				var comp = composition[i];

				resultData.push(
				{
					departureDate: comp.departureDate,
					trainName: comp.trainNumber,
					departureStation: comp.journeySections[0].beginTimeTableRow.stationUICCode,
					destinationStation: comp.journeySections[0].endTimeTableRow.stationUICCode,
					composition:
					{
						from: comp.journeySections[0].beginTimeTableRow.stationShortCode,
						to: comp.journeySections[0].endTimeTableRow.stationShortCode,
						wagons: comp.journeySections[0].wagons,
						locomotives: comp.journeySections[0].locomotives
					}

				});

			}

			res.send( JSON.stringify(resultData) );
		}
		else
		{
			res.send( JSON.stringify("DATA ERROR") );
		}

	});

});

module.exports = router;
