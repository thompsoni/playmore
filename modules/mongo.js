var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var config = require('../config');
var url = "mongodb://" + config.MONGO_HOST + ":" + config.MONGO_PORT + "/" + config.MONGO_DB;
		
exports.exec = function(id, arg, arg2)
{
	
	/**
	*	Create collection		 	 	
	*	@param {string} Name of the collection, ie. "users"
	*/
	if( id.localeCompare("create") === 0 )
	{

		//connect to DB
		MongoClient.connect(url, function(err, db) 
		{
		  if (err) 
		  {
			  console.log(err);
			  return 0;
		  }

				//create
				db.createCollection(arg, function(err, res) 
				{
					if (err) 
					{
						console.log(err);
						return 0;
					}
					console.log("-> Table " + arg + " created!");

					db.close();
					
				});
				
		});
		
	}
	
	/**
	*	List & print data		 	 	
	*	@param {string} Name of the collection, ie. "users"
	*/
	else if( id.localeCompare("list") === 0 )
	{
		//connect to DB
		MongoClient.connect(url, function(err, db) 
		{
			if (err) 
			{
				console.log(err);
				return 0;
			}
		  
			db.listCollections().toArray(function(err, info) 
			{
				info.forEach(function(item) 
				{
					console.log(item.name);
				});
			});
			
			db.close();

				
		});
		
	}
	
	
	/**
	*	Add data		 	 	
	*	@param {string} Name of the collection, ie. "users"
	*	@param {Object} Query, ie. { name: "Company Inc", address: "Highway 37" }
	*/
	else if( id.localeCompare("add") === 0 )
	{
		
		var collection = arg;	//ie. "users"
		var data = arg2;		//ie. { name: "Company Inc", address: "Highway 37" }
		
		MongoClient.connect(url, function(err, db) 
		{
			if (err) 
			{
				console.log(err);
				return 0;
			}
			  
			db.collection(collection).insert(data, function(err, res) 
			{
				if (err) 
				{
					console.log(err);
					return 0;
				}
				
				console.log("Added " + res.insertedCount + " rows to collection: " + collection);
				db.close();
			});
		  
		});
		
	}
	
	
	/**
	*	Get data		 	 	
	*	@param {string} Name of the collection, ie. "users"
	*	@param {Object} Query, ie. { address: "Park Lane 38" }
	*	@param {Object} Sort query, ie. { name: 1 } 1=asc, -1=desc
	*/
	else if( id.localeCompare("get") === 0 )
	{
		
		var collection = arg;	//ie. "users"
		var query = arg2;		//ie. { address: "Park Lane 38" }
		var sort = arg3;		//ie. { name: 1 } 1=asc, -1=desc
		
		MongoClient.connect(url, function(err, db) 
		{
			if (err) 
			{
				console.log(err);
				return 0;
			}
			  
			db.collection(collection).find(query).sort(sort).toArray(function(err, res) 
			{
				if (err) 
				{
					console.log(err);
					return 0;
				}
				
				console.log("Found " + res.length + " rows from collection: " + collection);
				db.close();
			});
		  
		});
		
	}
	
	
	/**
	*	Delete data		 	 	
	*	@param {string} Name of the collection, ie. "users"
	*	@param {Object} Query, ie. { address: "Park Lane 38" }
	*/
	else if( id.localeCompare("del") === 0 )
	{
		
		var collection = arg;	//ie. "users"
		var query = arg2;		//ie. { address: "Park Lane 38" }
		
		MongoClient.connect(url, function(err, db) 
		{
			if (err) 
			{
				console.log(err);
				return 0;
			}
			  
			db.collection(collection).remove(query, function(err, obj) 
			{
				if (err) 
				{
					console.log(err);
					return 0;
				}
				
				console.log("Deleted " + obj.result.n + " rows from collection: " + collection);
				db.close();
			});
		  
		});
		
	}
	
	
	/**
	*	Drop a collection		 	 	
	*	@param {string} Name of the collection, ie. "users"
	*/
	else if( id.localeCompare("drop") === 0 )
	{
		
		var collection = arg;	//ie. "users"
		
		MongoClient.connect(url, function(err, db) 
		{
			if (err) 
			{
				console.log(err);
				return 0;
			}
			  
			db.collection(collection).drop(function(err, delOK) 
			{
				if (err) 
				{
					console.log(err);
					return 0;
				}
				
				if (delOK)
				{
					console.log("Table " + collection + " deleted");
				}
				
				db.close();
			});
		  
		});
		
	}
}
