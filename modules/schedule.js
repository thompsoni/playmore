var config = require('../config');
const monk = require('monk');
const dburl = config.MONGO_HOST + ':' + config.MONGO_PORT + '/' + config.MONGO_DB;

/*
*   Loop all sessions and remove if older than config.SESSION_LIFETIME
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
exports.checkSessions = function()
{

  const db = monk(dburl);
  db.then(() =>
  {
      console.log('Connected correctly to server');

      const collection = db.get('sessions')
      collection.find({}).then((docs) =>
      {
          //list all session objs
          for(i=0; i<docs.length; i++)
          {
              var session_date = new Date(docs[i].date);	//session date
              var date_now = new Date();	//date now
              var date = (date_now - session_date) / 1000 / 60 / 60; //get difference in MS and convert to hours
              console.log("Session life: " + date);

              //remove if session time >= x hours
              if( date >= config.SESSION_LIFETIME )
              {
                  collection.remove(docs[i]);
                  console.log("-> deleted");
              }
          }

      }).catch((err) =>
      {
          // An error happened while inserting
          console.log(err);

      }).then(() =>
      {
          db.close();
      });

  });

}
