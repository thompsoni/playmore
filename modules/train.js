var config = require('../config');
var https = require('https');
var validator = require('validator');

//var liveTrains = null;                     //list of trains
//var trainCompositions = new Array();       //save compositions


/*exports.getTrains = function()
{
    return liveTrains;
}

exports.getCompositions = function()
{
    return trainCompositions;
}*/


/**
*   fetch trains data and get compositions
*   param station (str) station shortcut eg. "SLO"
*/
exports.fetchTrains = function(traincode, station, callback)
{

    var url = 'https://rata.digitraffic.fi/api/v1/live-trains?station=' + station;
    if( validator.isAlpha(station, 'en-US') !== true ){ console.log("fail: fetchtrains"); return false; }

    https.get(url, function(res)
    {
        var data = '';

        res.on('data', function(chunk)
        {
            data += chunk;
        });

        res.on('end', function()
        {
            var liveTrains = JSON.parse(data);
            var compObj = new Array();
            //console.log(liveTrains);

    		//get current date
    		var today = new Date();
    		var dd = today.getDate().toString();        if( dd.length <= 1 ){ dd = "0" + dd; }
    		var mm = (today.getMonth()+1).toString();   if( mm.length <= 1 ){ mm = "0" + mm; }
    		var yyyy = today.getFullYear();
    		var date = yyyy + "-" + mm + "-" + dd;

            var loops = 0;
            var found = false;


    		for( var i=0; i<liveTrains.length; i++ )
    		{
                for( var u=0; u<traincode.length; u++ )
        		{

                    console.log(i + ": " + liveTrains[i].trainNumber + " = " + traincode[u]);
        			if( liveTrains[i].trainNumber == traincode[u] )
                    {
                        found = true;

            			//getch compositions
            			fetchCompositions(traincode[u], date, function(comp)
                        {
                            if( comp === null ){ console.log("DATA ERROR"); }
                            callback( comp );

                        });

                        loops++;
                        if( loops >= traincode.length )
                        {
                            console.log("STOP");
                            return;
                        }
                    }


                }
    		}

            if( found === false )
            {
                callback( null );
            }
        });

    }).on('error', function(e)
    {
          callback( null );
          console.log("Error: ", e);
    });

};


/**
*   fetch train compositions
*   param train (int) train number
*   param date (str) date in format "yyyy-mm-dd"
*/
function fetchCompositions(train, date, callback)
{

    var url = 'https://rata.digitraffic.fi/api/v1/compositions/' + train + '?departure_date=' + date;
    if( validator.isNumeric(train.toString(), 'en-US') !== true ){ console.log("fail: fetchcomp"); return false; }
    if( validator.toDate(date) === null ){ console.log("fail: fetchcomp2"); return false; }

    https.get(url, function(res)
    {
        var data = '';

        res.on('data', function(chunk)
        {
            data += chunk;
        });

        res.on('end', function()
        {
            callback( JSON.parse(data) );
        });

    }).on('error', function(e)
    {
          callback( null );
          console.log("Error: ", e);
    });

};
