var config = require('../config');
const monk = require('monk');
const dburl = config.MONGO_HOST + ':' + config.MONGO_PORT + '/' + config.MONGO_DB;
const WebSocket = require('ws');
var wss = null;


/*
*  Start websocket
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
exports.startWS = function ()
{
    wss = new WebSocket.Server({ port: 8081 });
    console.log("Websocket started on port 8081");

};


/*
*  Broadcast something for all clients in real time
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
exports.startBroadcast = function ()
{

    if( wss != null )
    {
        //when user connects
        wss.on('connection', function connection(ws, req)
        {
            console.log("new connection from: " + req.headers.origin); //console.log(ws);
            //const location = url.parse(req.url, true);
            //console.log("CONNECTED: " + location);
        });

        //send data to all clients
        setInterval( () =>
        {
            wss.clients.forEach(function each(client)
            {
                if (client.readyState === WebSocket.OPEN)
                {
                    client.send("test123");
                }
            });
            console.log("sent to clients...");

        }, 10000);
    }
    else{ console.log("Websocket not started"); }

};
