var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var config = require('../config');
var url = "mongodb://" + config.MONGO_HOST + ":" + config.MONGO_PORT + "/" + config.MONGO_DB;
var sha256 = require('./sha256');


/**
*	Authenticate user by username & password.
*	Looks into 'users' collection in MongoDb and compares.
*	Passwords are hashed with SHA256.
*	@param username {string} Username, case sensitive
*	@param userpw {string} Password, case sensitive, sha256
*	@param callback {function} Runs this function after auth, either callback(true) or callback(false)
*/
exports.login = function(username, userpw, callback)
{
	
	var collection = 'users';
	var query = { name: username, password: userpw };
	//console.log("login: " + username + " " + userpw);
	
	MongoClient.connect(url, function(err, db) 
	{
		if (err) 
		{
			//console.log(err);
			callback(false);
			return false;
		}
		  
		db.collection(collection).find(query).toArray(function(err, res) 
		{
			if (err) 
			{
				//console.log(err);
				callback(false);
				return false;
			}
			
			//console.log("Found " + res.length + " rows from collection: " + collection);
			if( res.length >= 1 )
			{
				db.close();
				callback(true, res);
			}
			else
			{
				db.close();
				callback(false);
			}
			
		});
	  
	});
	
}


/**
*	Check whether session for the id already exists.
*	Returns true or false as a callback function.
*	@param userobj {Object} User object containing mongo userdata
*/
exports.sessionExists = function(userobj, callback)
{
	
	var collection = 'sessions';
	var query = { name: userobj[0].name };
	
	MongoClient.connect(url, function(err, db) 
	{
		if (err) 
		{
			//console.log(err);
			callback(false);
			return false;
		}
		  
		db.collection(collection).find(query).toArray(function(err, res) 
		{
			if (err) 
			{
				//console.log(err);
				callback(false);
				return false;
			}
			
			//console.log("Found " + res.length + " rows from collection: " + collection);
			if( res.length >= 1 )
			{
				db.close();
				callback(true, res);
			}
			else
			{
				db.close();
				callback(false, userobj);
			}
			
		});
	  
	});
	
}


/**
*	Create new session.
*	Return true or false, and session ID as callback function.
*	@param userobj {Object} User object containing mongo userdata
*/
exports.addSession = function(userobj, callback)
{
	
	var collection = 'sessions';
	var timestamp = new Date().getTime();
	var sessionid = sha256.hash( userobj[0].password + timestamp );
	var sessionObj = {name: userobj[0].name, sid: sessionid, date: timestamp};
	//console.log("NAME: " + userobj[0].name);
	
	MongoClient.connect(url, function(err, db) 
	{
		if (err) 
		{
			//console.log(err);
			callback(false);
			return false;
		}
		  
		db.collection(collection).insertOne(sessionObj, function(err, res) 
		{
			if (err) 
			{
				//console.log(err);
				callback(false);
				return false;
			}
			
			db.close();
			callback(true, sessionid);
			
		});
	  
	});
	
}


/**
*	Check if the session exists.
*	Returns true or false as a callback function.
*	@param sid {string} session id
*/
exports.verifySession = function(sid, callback)
{
	
	var collection = 'sessions';
	var query = { sid: sid };
	
	MongoClient.connect(url, function(err, db) 
	{
		if (err) 
		{
			//console.log(err);
			callback(false);
			return false;
		}
		  
		db.collection(collection).find(query).toArray(function(err, res) 
		{
			if (err) 
			{
				//console.log(err);
				callback(false);
				return false;
			}
			
			//console.log("Found " + res.length + " rows from collection: " + collection);
			if( res.length >= 1 )
			{
				db.close();
				callback(true, res);
			}
			else
			{
				db.close();
				callback(false);
			}
			
		});
	  
	});
	
}


